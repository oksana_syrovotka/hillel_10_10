# # Заданий рядок зі словами і числами, розділеними пробілами (один пробіл між словами та / або числами).
# # Слова складаються тільки з букв. Потрібно перевірити чи є у вихідному рядку три слова підряд.
# # Наприклад, в рядку "start 5 one two three 7 end" є три слова підряд.
# # Результат має бути булеве True або False



words = "start 5 one 5  5 7 ine one three 7 end"
count = 0
rez = False


for word in words.split():
    if word.isalpha():
        count += 1
        if count == 3: 
             rez = True
             break
    else:
        count = 0 

print(rez)

