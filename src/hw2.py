# Перевірити, що аргументи є
# Знайти серед аргументів файл із розширенням .xls
# Знайти серед аргументів модифікатори (починаються з "--" ), ключі (починаються з "-") і  значення ключів (наступний елемент із списку за ключем) 
# Вивести в ствопчик знайдені в попередньому пункі елементи із відповідним тегом, наприклад "key: -c", "flag: --f", "key_value: True"


import sys
import re


arg = sys.argv[1:]


# 1
if len(arg) < 1:
    print('\n 1.ERROR: there are no arguments')
else:
    print('\n 1.There are arguments')




# 2
xls_file = []
pattern = r"\w+\.xls"


for argument in arg:
    matches = re.findall(pattern, argument)
    xls_file.extend(matches)

if xls_file:
    print(f'\n 2.File with .xls extension: {xls_file}')
else:
    print('\n 2.No .xls files found')



# 3
pattern2 = r"--\w+"
pattern3 = r"-\w+"

matches2 = []
matches3 = []


for argument in arg:
    if re.findall(pattern2, argument):
        matches2.append(argument)
    elif re.findall(pattern3, argument):
        matches3.append(argument)


print("\n 3.Flag --:")
for item in matches2:
    print(item)

print("\n 4.Flag -:")
for item in matches3:
    print(item)













